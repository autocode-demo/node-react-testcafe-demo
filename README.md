### Autocode e2e tests example with TestCafe (Node.js build tool)
* Simple React app that displays React logo
* All tests should be located in `test` folder
* npm scripts executed by Autocode: 
  * Compile stage: `npm run build`
  * Tests stage: `npm run test`
* npm test script breakdown:
  * `chromium:headless` - executes tests in Chromium browser (headless mode). Only Chromium is supported.
  * `--reporter xunit:junit.xml` - sets reporter format to "xunit" with "junit.xml" as output file. Autocode uses this exact file as a test report
  * `--app 'http-server ./build -s'` - launches a static web server under ./build folder before tests. The web server is available at :8080 port by default.
